package beerAdvocate.top100;

public enum TopBeersEnum {
	Id(1),
	Name(2),
	StyleId(3),
	Type(4),
	ABV(5),
	BreweryId(6),
	BreweryName(7),
	Rating(8), ;

	public int index;

	private TopBeersEnum(int index) {
		this.index = index;
	}
	
	
}
