package beerAdvocate.beerDetails;

import beerAdvocate.parser.Beer;

public class BeerDetails extends Beer {
	private Float brosRating;
	private Float rAverage;
	private Float pDeviation;
	private Float high;
	private Float low;
	private Float rWeight;
	private Float psDeviation;

	public Float getBrosRating() {
		return brosRating;
	}

	public void setBrosRating(Float brosRating) {
		this.brosRating = brosRating;
	}

	public Float getrAverage() {
		return rAverage;
	}

	public void setrAverage(Float rAverage) {
		this.rAverage = rAverage;
	}

	public Float getpDeviation() {
		return pDeviation;
	}

	public void setpDeviation(Float pDeviation) {
		this.pDeviation = pDeviation;
	}

	public Float getHigh() {
		return high;
	}

	public void setHigh(Float high) {
		this.high = high;
	}

	public Float getLow() {
		return low;
	}

	public void setLow(Float low) {
		this.low = low;
	}

	public Float getrWeight() {
		return rWeight;
	}

	public void setrWeight(Float rWeight) {
		this.rWeight = rWeight;
	}

	public Float getPsDeviation() {
		return psDeviation;
	}

	public void setPsDeviation(Float psDeviation) {
		this.psDeviation = psDeviation;
	}

	@Override
	public String toString() {
		return super.toString() + " brosRating:" + this.brosRating + " rAverage:" + this.rAverage;
	}
}
