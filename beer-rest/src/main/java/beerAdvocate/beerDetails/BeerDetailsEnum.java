package beerAdvocate.beerDetails;



public enum BeerDetailsEnum {
	Name(1),
	Rating(2),
	BrosRating(3),
	RAverage(4),
	RWeight(5),
	PsDeviation(6),
	PDeviation(7),
	High(8),
	Low(9),
	BreweryName(10),
	StyleId(11),
	StyleType(12),
	Abv(13);

	//private static Logger log = LoggerFactory.getLogger(BeerDetailsEnum.class);

	public int index;

	private BeerDetailsEnum(int index) {
		this.index = index;
	}

	/**
	 * Used to handle the "N/A" rating and turn them into nulls
	 * @param rating
	 * @return
	 */
	public static Float normalizeRating(String rating) {
		Float result = null;
		if (!"N/A".equals(rating)) {
			try {
				result = Float.parseFloat(rating);
			} catch (Exception ex) {
				//log.warn("Catching a parse", ex);
			}
		}
		return result;
	}
}
