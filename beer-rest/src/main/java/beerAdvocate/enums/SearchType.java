package beerAdvocate.enums;

public enum SearchType {
	Beer("beer"), Place("place"), Event("event");

	public final String query;

	private SearchType(String query) {
		this.query = query;
	}
}
