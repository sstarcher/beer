package beerAdvocate.parser;

import java.util.List;

import com.google.gson.Gson;

import beerAdvocate.top100.Brewery;


public class Beer {
	private int id;
	private String name;
	private Float rating;

	public Beer() {

	}

	public Beer(int id, String name, int styleId, List<String> type, Float abv, Brewery brewery, Float rating) {
		this.setId(id);
		this.setName(name);
		this.setRating(rating);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public Float getRating() {
		return rating;
	}

	public void setRating(Float rating) {
		this.rating = rating;
	}

	@Override
	public String toString() {
		return new Gson().toJson(this, Beer.class);
	}
}
