package beerAdvocate.parser;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beerAdvocate.HttpHandler;
import beerAdvocate.beerDetails.BeerDetails;
import beerAdvocate.beerDetails.BeerDetailsEnum;
import beerAdvocate.enums.SearchType;
import beerAdvocate.search.SearchEnum;
import beerAdvocate.top100.TopBeersEnum;

public class BeerAdvocate {
	private static Logger log = LoggerFactory.getLogger(BeerAdvocate.class);

	public static void main(String[] args) throws MalformedURLException, IOException {

		// log.error("total beers:" + new
		// BeerAdvocate().getTopBeerList().size());
		for (Beer beer : new BeerAdvocate().search("test", SearchType.Beer)) {
			log.error(beer.getName());
		}
	}

	public List<Beer> getTopBeerList() throws MalformedURLException, IOException {
		List<Beer> top100 = new ArrayList<Beer>();

		String result = HttpHandler.getUrl("http://beeradvocate.com/lists/popular");

		Pattern regex = Pattern
				.compile(
						"<a href=\"/beer/profile/\\d*/(\\d*?)\"><b>(.*?)</b></a><br><a href=\"/beer/style/(\\d*?)\">([\\w\\s\\d/()-]*?)</a>(?: / (.*?)% ABV)?<br><a href=\"/beer/profile/(\\d*)\">(.*?)</a>.*?<b>(.*?)</span>.*?<b>(\\d*)",
						Pattern.DOTALL);
		Matcher regexMatcher = regex.matcher(result);
		while (regexMatcher.find()) {
			Beer beer = new Beer();
			top100.add(beer);
			for (TopBeersEnum value : TopBeersEnum.values()) {
				String regexValue = regexMatcher.group(value.index);
				switch (value) {
				case Id:
					beer.setId(Integer.parseInt(regexValue));
					break;
				case Name:
					beer.setName((String) regexValue);
					break;
				case Rating:
					beer.setRating(Float.parseFloat(regexValue));
					break;
				default:
					log.error("WHY did we not specify everything?");
				}
			}
		}
		if (top100.size() != 100) {
			log.warn("This list is suppose to be 100 long we are likely not parsing it correctly?");
		}
		return top100;
	}

	public List<Beer> search(String query, SearchType type) throws MalformedURLException, IOException {
		if (!SearchType.Beer.equals(type)) {
			throw new RuntimeException("Lol you thought this was implemented");
		}

		List<Beer> searchResults = new ArrayList<Beer>();
		String response = HttpHandler.getUrl("http://beeradvocate.com/search?q=" + query + "&qt=" + type.query);

		Pattern regex = Pattern.compile(
				"<li>.*?<a href=\"/beer/profile/(\\d*)/(\\d*)\"><b>(.*?)</b>.*?\">(.*?)</a>.*?#666666;\">\\|\\s?([\\w\\s,]*)</span></li>",
				Pattern.DOTALL);

		Matcher regexMatcher = regex.matcher(response);
		while (regexMatcher.find()) {
			BeerDetails beer = new BeerDetails();

			searchResults.add(beer);
			for (SearchEnum value : SearchEnum.values()) {
				String regexValue = regexMatcher.group(value.index);
				switch (value) {

				case Id:
					beer.setId(Integer.parseInt(regexValue));
					break;
				case Name:
					beer.setName(regexValue);
					break;
				default:
					log.warn("Why do we have extra?");
				}
			}
			log.error("beer:" + beer.toString());
		}
		return searchResults;

	}

	public BeerDetails get(int breweryId, int beerId) throws MalformedURLException, IOException {
		BeerDetails beer = new BeerDetails();

		String response = HttpHandler.getUrl("http://beeradvocate.com/beer/profile/" + breweryId + "/" + beerId);

		Pattern regex = Pattern
				.compile(
						"\"norm\">(.*)</h1>.*?\"BAscore_big\">(.*?)</span>.*?\"BAscore_big\">(.*?)</span>.*?rAvg: (.*?)<!--<br>WR: (.*?)-->.*?psDev: (.*?)%-->.*?pDev: (.*?)%.*?High: (.*?)<br>Low: (.*?)<br>.*?/beer/profile/.*?\"><b>(.*?)</b>.*?/beer/style/(\\d*)\"><b>(.*?)</b>.*?&nbsp;(.*?)% <a href",
						Pattern.DOTALL);

		Matcher regexMatcher = regex.matcher(response);
		while (regexMatcher.find()) {
			beer.setId(beerId);
			log.error("EHH:" + regexMatcher.group(10));
			for (BeerDetailsEnum value : BeerDetailsEnum.values()) {
				String regexValue = regexMatcher.group(value.index);
				switch (value) {
				case BrosRating:
					beer.setBrosRating(BeerDetailsEnum.normalizeRating(regexValue));
					break;
				case High:
					beer.setHigh(BeerDetailsEnum.normalizeRating(regexValue));
					break;
				case Low:
					beer.setLow(BeerDetailsEnum.normalizeRating(regexValue));
					break;
				case Name:
					beer.setName(regexValue);
					break;
				case PDeviation:
					beer.setpDeviation(BeerDetailsEnum.normalizeRating(regexValue));
					break;
				case PsDeviation:
					beer.setPsDeviation(BeerDetailsEnum.normalizeRating(regexValue));
					break;
				case Rating:
					beer.setRating(BeerDetailsEnum.normalizeRating(regexValue));
					break;
				case RAverage:
					beer.setrAverage(BeerDetailsEnum.normalizeRating(regexValue));
					break;
				case RWeight:
					beer.setrWeight(BeerDetailsEnum.normalizeRating(regexValue));
					break;
				default:
					log.warn("Why do we have extra?");
				}

			}
			log.error("beer:" + beer.toString());
		}
		return beer;

	}

	public void get(int breweryId) throws MalformedURLException, IOException {
		String response = HttpHandler.getUrl("http://beeradvocate.com/beer/profile/" + breweryId);
		log.error(response);
	}

}
