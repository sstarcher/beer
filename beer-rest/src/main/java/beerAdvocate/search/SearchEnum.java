package beerAdvocate.search;

public enum SearchEnum {
	BreweryId(1), Id(2), Name(3), BreweryName(4), Location(5);

	public final int index;

	private SearchEnum(int index) {
		this.index = index;
	}
}
