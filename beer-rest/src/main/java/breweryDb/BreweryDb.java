package breweryDb;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URLEncoder;

import beerAdvocate.HttpHandler;
import breweryDb.data.Data;
import breweryDb.data.Result;

import com.google.gson.Gson;

public class BreweryDb {
	private static String key="339ee64e5bab80094ecc82598fbc3b6b";
	
	public static void main(String[] args) throws MalformedURLException, IOException {
		String name ="Pliny the Younger" ;
		Data beer = new BreweryDb().getBeerByName(name);
		System.err.println(beer.getName());
		System.err.println(beer);
	}
	
	public Data getBeerByName(String name) throws MalformedURLException, IOException{
		Data data = null;
		String urlEncodedName = URLEncoder.encode(name, "UTF-8");
		String response = HttpHandler.getUrl("http://api.brewerydb.com/v2/beer?key="+key+"&name="+urlEncodedName);
		
		Result beer = new Gson().fromJson(response, Result.class);
		if(beer.getData()!=null){
			data = beer.getData().get(0);
		}else{
			
		}
		return  data;
	}
}
