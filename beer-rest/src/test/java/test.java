import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import org.junit.Test;

import beerAdvocate.parser.BeerAdvocate;
import breweryDb.BreweryDb;
import breweryDb.data.Data;

import static org.junit.Assert.*;

public class test {

	@Test
	public void top100Beers() throws MalformedURLException, IOException {
		assertEquals(new BeerAdvocate().getTopBeerList().size(), 100);
	}
	
	@Test
	public void getTop100CheckBreweryDb() throws MalformedURLException, IOException{
		BreweryDb db = new BreweryDb();
		List<beerAdvocate.parser.Beer> beers = new BeerAdvocate().getTopBeerList();
		int counter =0;
		for(beerAdvocate.parser.Beer beer : beers){
			Data data = db.getBeerByName(beer.getName());
			if(data==null){
				counter++;
				System.err.println(beer.getName());
			}
		}
		System.err.println("counter:"+counter);
	}
}
