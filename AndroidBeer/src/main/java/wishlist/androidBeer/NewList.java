package wishlist.androidBeer;

import wishlist.androidBeer.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NewList extends Activity {
	public static final String NEW_LIST_NAME = "ListName";
	
	private EditText listNameText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_list_window);
		
		listNameText = (EditText)findViewById(R.id.listName);
		
		Button confirmButton = (Button)findViewById(R.id.createButton);
		
		confirmButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 
				String name = listNameText.getText().toString();
				
				if(name.isEmpty())
				{
					InvalidName();
					return;
				}
				
				Bundle resultBundle = new Bundle();
				
				resultBundle.putString(NEW_LIST_NAME, name);
				
				Intent returnIntent = new Intent();
				returnIntent.putExtras(resultBundle);
				
				setResult(RESULT_OK, returnIntent);
				finish();
			}
		});
	}
	
	private void InvalidName()
	{
		Toast.makeText(this, "Invalid Name", Toast.LENGTH_SHORT).show();
	}

}
