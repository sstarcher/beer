package wishlist.androidBeer.data;

import java.util.ArrayList;
import java.util.Collection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class BeerList {
	@DatabaseField(generatedId = true)
	private long id;
	
	public long getId() {
		return id;
	}

	@DatabaseField()
	private String name;

	@ForeignCollectionField()
	private Collection<BeerListData> beers;
	
	public Collection<BeerListData> getBeers() {
		if(beers == null)
			beers = new ArrayList<BeerListData>();
		
		return beers;
	}

	public void setBeers(Collection<BeerListData> beers) {
		this.beers = beers;
	}

	public BeerList() {
		
	}
	
	public void setName(String newName) throws NameInvalidException
	{
		if(newName.isEmpty())
			throw new NameInvalidException();
		
		name = newName;
	}
	
	
	public class NameInvalidException extends Exception
	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
	}


	public CharSequence getName() {
		// TODO Auto-generated method stub
		return name;
	}
	
	public void addBeer(BeerListData toAdd)
	{
		this.getBeers().add(toAdd);
	}
	
	
}
