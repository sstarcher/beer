package wishlist.androidBeer.data;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class BeerListData {
	@DatabaseField(generatedId = true)
	private long id;
		
	@DatabaseField(foreign = true)
	private BeerList list;
	
	@DatabaseField(foreign = true)
	private Beer beer;
	
	public long getId() {
		return id;
	}

	public BeerList getList() {
		return list;
	}

	public void setList(BeerList list) {
		this.list = list;
	}

	public Beer getBeer() {
		return beer;
	}

	public void setBeer(Beer beer) {
		this.beer = beer;
	}

	public BeerListData()
	{
	}
	
	public BeerListData(BeerList list, Beer beer)
	{
		this.list = list;
		this.beer = beer;
	}
}
