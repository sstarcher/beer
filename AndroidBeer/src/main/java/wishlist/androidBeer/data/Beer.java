package wishlist.androidBeer.data;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Beer {

	@DatabaseField(generatedId = true)
	private long id;
	
	@DatabaseField(canBeNull = false, defaultValue = "false")
	private Boolean userHad;
	
	@DatabaseField(canBeNull = false)
	private String name;
	
	public Beer() {}
	
	public Beer(beerAdvocate.parser.Beer toParse)
	{
		this.name = toParse.getName();
	}

	public Boolean getUserHad() {
		return userHad;
	}

	public void setUserHad(Boolean userHad) {
		this.userHad = userHad;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
