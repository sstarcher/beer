package wishlist.androidBeer.adapters;

import java.util.List;

import wishlist.androidBeer.R;
import wishlist.androidBeer.data.Beer;
import wishlist.androidBeer.data.BeerListData;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

public class BeerListDataListAdapter extends BaseAdapter {

	private Context context;
	private List<BeerListData> data;
	private int row;
	private LayoutInflater inflator;

	public BeerListDataListAdapter(Context context,	List<BeerListData> objects) {
		super();
		 
		this.context = context;
		this.data = objects;
		
		this.inflator = LayoutInflater.from(context);
	}
	//TODO: Add Listener
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null)
		{
			convertView = inflator.inflate(R.layout.beer_item_check_list, null);
			
			holder = new ViewHolder();
			holder.checkbox = (CheckBox)convertView.findViewById(R.id.BeerListItem);
			
			convertView.setTag(holder);
		}
		else{
			holder = (ViewHolder)convertView.getTag();
		}
		
		BeerListData listData = data.get(position);
		
		try
		{
		Beer beer = listData.getBeer();
		
		//DatabaseHelper dbHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
		//dbHelper.getBeerDao().refresh(beer);
		
		holder.checkbox.setChecked(beer.getUserHad());
		holder.checkbox.setText(beer.getName());
		}
		catch(NullPointerException e)
		{
			Log.e("BeerLIstDataAdapter", "Got Null Values for BeerListData id: " + listData.getId());
			
			Beer possibleBeer = (Beer)listData.getList().getBeers().toArray()[position];
			
			Log.i("BeerListDataAdapter", "Try two with name: " + possibleBeer.getName());
		}
		
		return convertView;
		
	}
	
	static class ViewHolder
	{
		CheckBox checkbox;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	//Returning the index passed in was taken from google's list14 example
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	//See above
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

}
