package wishlist.androidBeer.adapters;

import java.util.List;

import wishlist.androidBeer.data.BeerList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

//TODO: Switch over to the Holder Pattern for more efficient
public class ListNameListAdapter extends ArrayAdapter<BeerList> {
	

	private Context context;
	private int row;
	private List<BeerList> list;

	public ListNameListAdapter(Context context, int textViewResourceId,
			List<BeerList> objects) {
		super(context, textViewResourceId, objects);
		
		this.context = context;
		this.row = textViewResourceId;
		this.list = objects;
		
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView view;
		
		if(convertView == null)
		{
			convertView = new TextView(context);
		}
		else{
			//Nothing here for now
		}
		
		view = (TextView)convertView;
		
		view.setText(list.get(position).getName());
		view.setTag(list.get(position).getId());
		
		return view;
		
	}

	
}
