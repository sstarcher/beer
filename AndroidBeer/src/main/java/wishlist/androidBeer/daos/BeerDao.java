package wishlist.androidBeer.daos;

import java.util.List;

import wishlist.androidBeer.data.Beer;

public interface BeerDao {
	
	void create(Beer toCreate);
	
	void update(Beer toUpdate);
	
	Beer getById(long id) throws DaoException;
	
	Beer getByName(String name) throws DaoException;
	
	List<Beer> getAll();
}
