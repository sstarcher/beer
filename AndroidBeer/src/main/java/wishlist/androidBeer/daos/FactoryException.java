package wishlist.androidBeer.daos;

public class FactoryException extends Exception
{
	public FactoryException(Exception inner)
	{
		super(inner);
	}
}