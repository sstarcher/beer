package wishlist.androidBeer.daos;

import java.util.List;

import wishlist.androidBeer.data.BeerList;

public interface ListDao {
	
	void create(BeerList toCreate);
	
	void update(BeerList toUpdate);
	
	BeerList getById(long id);
	
	BeerList getByName(String Name) throws DaoException;
	
	List<BeerList> getAll() throws DaoException;
}

