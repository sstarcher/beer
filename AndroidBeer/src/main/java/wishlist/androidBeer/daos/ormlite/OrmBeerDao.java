package wishlist.androidBeer.daos.ormlite;
import java.sql.SQLException;
import java.util.List;

import wishlist.androidBeer.daos.BeerDao;
import wishlist.androidBeer.data.Beer;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;

public class OrmBeerDao implements BeerDao{
	private Dao<Beer, Long> beerDao;
	
	public OrmBeerDao(OrmLiteSqliteOpenHelper helper) throws SQLException
	{
		this.beerDao =  helper.getDao(Beer.class);
	}
	
	public void update(Beer toUpdate) {
		// TODO Auto-generated method stub
		
	}
	
	public void create(Beer toCreate)
	{
		
	}

	
	public Beer findByName(String name) throws SQLException
	{
		QueryBuilder<Beer,Long> query = beerDao.queryBuilder();
		SelectArg nameArg = new SelectArg();
		
		nameArg.setValue(name);
		query.where().eq("name",nameArg);
		
		return beerDao.queryForFirst(query.prepare());
	}

	@Override
	public Beer getById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Beer getByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Beer> getAll() {
		// TODO Auto-generated method stub
		return null;
	}
}
