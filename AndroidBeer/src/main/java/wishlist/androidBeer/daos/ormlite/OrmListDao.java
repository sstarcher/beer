package wishlist.androidBeer.daos.ormlite;

import java.sql.SQLException;
import java.util.List;

import wishlist.androidBeer.daos.DaoException;
import wishlist.androidBeer.daos.ListDao;
import wishlist.androidBeer.data.BeerList;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

public class OrmListDao implements ListDao {
	Dao<BeerList, Long> listDao;
	
	public OrmListDao(OrmLiteSqliteOpenHelper helper) throws SQLException
	{
		listDao = helper.getDao(BeerList.class);
	}

	public BeerList getById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	public BeerList getByName(String name) throws DaoException {
		try
		{
			QueryBuilder<BeerList,Long> builder = listDao.queryBuilder();
			builder.where().eq("name", name);
			
			return listDao.queryForFirst(builder.prepare());
		}catch (SQLException e) {
			// TODO: handle exception
			throw new DaoException(e);
		} finally {
		}
	}

	public List<BeerList> getAll() throws DaoException {
		// TODO Auto-generated method stub
		try {
			return listDao.queryForAll();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			throw new DaoException(e);
		}
	}

	public void create(BeerList toCreate) {
		// TODO Auto-generated method stub
	}
	
	public void update(BeerList toUpdate)
	{
		
	}
}
