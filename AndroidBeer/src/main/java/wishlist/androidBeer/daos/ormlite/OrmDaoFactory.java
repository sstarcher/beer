package wishlist.androidBeer.daos.ormlite;

import java.sql.SQLException;
import wishlist.androidBeer.daos.BeerDao;
import wishlist.androidBeer.daos.DaoFactory;
import wishlist.androidBeer.daos.FactoryException;
import wishlist.androidBeer.daos.ListDao;
import wishlist.androidBeer.data.Beer;
import wishlist.androidBeer.data.BeerList;
import wishlist.androidBeer.data.BeerListData;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class OrmDaoFactory extends OrmLiteSqliteOpenHelper implements DaoFactory {
	private static final String DB_NAME = "Beer.db";
	private static final int DB_VERSION = 1;
	private static final String TAG = "DatabaseHelper";
	
	private OrmListDao listDao;
	private Dao<BeerListData, Integer> beerDataDao;
	private OrmBeerDao beerDao;

	public OrmDaoFactory(Context context)
	{
		super(context, DB_NAME, null, DB_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connection) {
		// TODO Auto-generated method stub
		Log.i(TAG, "onCreate");
		try {
			TableUtils.createTable(connection, BeerList.class);
			TableUtils.createTable(connection, BeerListData.class);
			TableUtils.createTable(connection, Beer.class);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error Creating Database", e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, ConnectionSource arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub
		Log.i(TAG, "Need To Implememnt");

	}
	
	
	public ListDao getListDao() throws FactoryException
	{
		if(this.listDao == null)
			try {
				listDao = new OrmListDao(this);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				throw new FactoryException(e);
			}
		
		return this.listDao;
	}
	
	public BeerDao getBeerDao() throws FactoryException
	{
		try
		{
			if(beerDao == null)
			beerDao = new OrmBeerDao(this);
		}
		catch (SQLException e) {
			throw new FactoryException(e);
		}
		
		return beerDao;
	}
	
	
	public Dao<BeerListData, Integer> getBeerDataDao() throws SQLException
	{
		if(this.beerDataDao == null)
			beerDataDao = DaoManager.createDao(getConnectionSource(), BeerListData.class);
	
		return beerDataDao;
	}
	

}
