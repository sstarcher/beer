package wishlist.androidBeer.daos;

public interface DaoFactory {
	
	BeerDao getBeerDao() throws FactoryException;
	
	ListDao getListDao() throws FactoryException;

}
