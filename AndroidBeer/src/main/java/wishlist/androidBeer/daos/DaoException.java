package wishlist.androidBeer.daos;

public class DaoException extends Exception {
	public DaoException(Exception inner)
	{
		super(inner);
	}
}
