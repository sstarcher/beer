package wishlist.androidBeer;

import android.app.Application;
import android.content.Context;

public class AndroidBeerApp extends Application {
	private static Context context;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		AndroidBeerApp.context = getApplicationContext();
	}
	
	public static Context getAppContext()
	{
		return AndroidBeerApp.context;
	}

}
