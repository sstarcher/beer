package wishlist.androidBeer;

import java.util.ArrayList;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import wishlist.androidBeer.adapters.BeerListDataListAdapter;
import wishlist.androidBeer.daos.FactoryException;
import wishlist.androidBeer.daos.ormlite.OrmDaoFactory;
import wishlist.androidBeer.data.BeerList;
import wishlist.androidBeer.data.BeerListData;



import android.app.ListActivity;
import android.os.Bundle;

public class CatagoryList extends ListActivity {
	private static Logger log = LoggerFactory.getLogger(CatagoryList.class);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		List<BeerListData> beers = null;

		Bundle extras = getIntent().getExtras();

		if (extras != null) {
			long listID = extras.getLong("LIST_ID");
			
			
			OrmDaoFactory helper = new OrmDaoFactory(this);
			BeerList list = null;
			try {
				list = helper.getListDao().getById(listID);
			} catch (FactoryException e) {
				// TODO Auto-generated catch block
				log.error("Shit went bad getting list");
				e.printStackTrace();
			}
			
			beers = new ArrayList<BeerListData>(list.getBeers());
			log.debug(Integer.toString(beers.size()) + " beers in list: " + list.getName());

		}
		
		BeerListDataListAdapter adapter = new BeerListDataListAdapter(this, beers);

		setListAdapter(adapter);

	}

}
