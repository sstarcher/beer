package wishlist.androidBeer;

import java.sql.SQLException;
import java.util.List;

import wishlist.androidBeer.adapters.ListNameListAdapter;
import wishlist.androidBeer.daos.BeerDao;
import wishlist.androidBeer.daos.DaoException;
import wishlist.androidBeer.daos.FactoryException;
import wishlist.androidBeer.daos.ListDao;
import wishlist.androidBeer.daos.ormlite.OrmBeerDao;
import wishlist.androidBeer.daos.ormlite.OrmDaoFactory;
import wishlist.androidBeer.data.BeerList;
import wishlist.androidBeer.data.BeerListData;
import wishlist.androidBeer.data.BeerList.NameInvalidException;

import beerAdvocate.parser.Beer;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import wishlist.androidBeer.R;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

public class MainListActivity extends ListActivity {
	private static final String TAG = "MainListActivity";
	private static final int ADD_ID = Menu.FIRST;
	private static final int DICKS_ID = Menu.FIRST + 1;
	
	private static final int ACTIVITY_CREATE =0;
	
	private OrmDaoFactory dbHelper;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		dbHelper = OpenHelperManager.getHelper(this, OrmDaoFactory.class);
		
		//DEBUG test code that can be removed later
		try {
			long listCount = dbHelper.getListDao().getAll().size();
			
		} catch (FactoryException e) {
			Log.e(TAG, "Shit went bad getting list dao.", e);
			throw new RuntimeException(e);
		} catch (DaoException e) {
			Log.e(TAG, "Shit went bad getting all lists.", e);
			throw new RuntimeException(e);
		}
		
		checkAndBuildDefaults();
		setContentView(R.layout.main);
		
		fillData();
		//String[] beerLists = getResources().getStringArray(R.array.beerLists);
		
		
		//setListAdapter(new ArrayAdapter<String>(this, R.layout.beer_list_catagory_list_item, beerLists));
		
		//Get the list view that represents this
		//ListView view = getListView();
		
		//turn on text filtering
		//view.setTextFilterEnabled(true);
			
	}
	
	private void checkAndBuildDefaults()
	{	
		BeerList beerAdvocate;
		ListDao listDao;
		String beerAdvocateName = getString(R.string.BeerAdvocateTopList);
		
		try
		{
		listDao = dbHelper.getListDao();
		
		beerAdvocate = listDao.getByName(beerAdvocateName);
		}
		catch(FactoryException e)
		{
			Log.e(TAG, "Error trying to Find Beer Advocate List");
			return; //Throw?
		} catch (DaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		
		if( beerAdvocate == null)
		{
			beerAdvocate = new BeerList();
			try {
				beerAdvocate.setName(beerAdvocateName);
			} catch (NameInvalidException e1) {
				Log.e(TAG, "How the fuck did this fail.");
				return; //throw?
			}
			
			
			try {
				listDao.create(beerAdvocate);
				List<Beer> top100 = new beerAdvocate.parser.BeerAdvocate().getTopBeerList();
				BeerDao beerDao = dbHelper.getBeerDao();
				Dao<BeerListData, Integer> beerDataDao = dbHelper.getBeerDataDao();
				wishlist.androidBeer.data.Beer dbBeer = null;
				for (Beer beer : top100) {
					dbBeer = beerDao.getByName(beer.getName());
					
					if(dbBeer == null)
					{
						dbBeer = new wishlist.androidBeer.data.Beer(beer);
						beerDao.create(dbBeer);
					}
					
					BeerListData dataToAdd = new BeerListData(beerAdvocate, dbBeer);
					beerDataDao.create(dataToAdd);
					beerAdvocate.addBeer(dataToAdd);
				}
				listDao.update(beerAdvocate);
				
			} catch (Exception e) {
				Log.e(TAG,"Fail", e);
			}
		}
	}
	
	private void fillData()
	{
		List<BeerList> lists;
		try {
			lists = dbHelper.getListDao().getAll();
			Log.i(TAG, "Currently " + Integer.toString(lists.size()) + " in DB");
		} catch (FactoryException e) {
			Log.e(TAG, "Error during fillData", e);
			throw new RuntimeException(e);
		} catch (DaoException e) {
			Log.e(TAG, "Error during fillData getting all lists", e);
			throw new RuntimeException(e);
		}
		
		//TODO: Might want to make a custom adapter for this
		
		ListNameListAdapter adapter = new ListNameListAdapter(this, R.layout.beer_list_catagory_list_item, lists);
		setListAdapter(adapter);
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		super.onCreateOptionsMenu(menu);
		menu.add(Menu.NONE, ADD_ID, Menu.NONE, "New");
		menu.add(Menu.NONE, DICKS_ID, Menu.NONE, "Dick");
		return true;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		
		Intent listIntent = new Intent(this, CatagoryList.class);
		long listId = (Long) v.getTag();
		
		listIntent.putExtra("LIST_ID", listId);
		
		startActivity(listIntent);
		}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		// TODO Auto-generated method stub
		super.onMenuItemSelected(featureId, item);
		
		switch(item.getItemId())
		{
		case ADD_ID:
			{
				Intent i = new Intent(this,NewList.class);
				startActivityForResult(i, ACTIVITY_CREATE);
				break;
			}
		case DICKS_ID:
			{
				Intent i = new Intent(this, NewList.class);
				startActivity(i);
				break;
			}
		}
		
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		Bundle extras = data.getExtras();
		
		switch(requestCode)
		{
		case ACTIVITY_CREATE:
			if(resultCode != RESULT_OK)
				return;
			
			String newListName = extras.getString(NewList.NEW_LIST_NAME);
			
			BeerList newList = new BeerList();
			try {
				newList.setName(newListName);
				ListDao dao = dbHelper.getListDao();
				dao.create(newList);
				fillData();
			} catch (NameInvalidException e) {
				Log.e(TAG, "Invalid name for new List");
				e.printStackTrace();
			} catch (FactoryException e) {
				Log.e(TAG, "Dao Error setting new list", e);
				e.printStackTrace();
			}
			
			
		}
	}
		
}
