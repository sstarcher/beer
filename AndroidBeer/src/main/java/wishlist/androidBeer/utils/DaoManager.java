package wishlist.androidBeer.utils;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import wishlist.androidBeer.AndroidBeerApp;
import wishlist.androidBeer.daos.DaoFactory;
import wishlist.androidBeer.daos.ormlite.OrmDaoFactory;

public class DaoManager {
	private static DaoManager manager;
	
	public static DaoManager getInstance()
	{
		if (manager == null)
			manager = new DaoManager();
		
		return manager;
	}
	
	private DaoManager()
	{
		
	}
	
	private DaoFactory factory;
	
	public DaoFactory getFactory()
	{
		if(factory == null)
		{
			factory = OpenHelperManager.getHelper(AndroidBeerApp.getAppContext(), OrmDaoFactory.class);
		}
		
		return factory;
	}
}
